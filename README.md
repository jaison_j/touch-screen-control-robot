# touch Screen control Robot

An embedded system is a combination of computer circuitry and software that is 
built into a product for purposes such as control, monitoring and communication 
without human intervention. Embedded systems are at the core of every modern
electronic product, ranging from toys to medical equipment to aircraft control 
systems. Embedded systems span all aspects of modern life and there are many
examples of their use. The uses of embedded systems are virtually limitless, 
because every day new products are introduced to the market that utilizes 
embedded system in novel ways.
An embedded system contains at least one microprocessor which performs the 
logic operations for the system. Many embedded systems use one or more
microcontrollers, which are a type of microprocessor that emphasizes
self-sufficiency and cost-effectiveness, instead of a general-purpose
microprocessor. A typical microcontroller contains sufficient memory 
and interfaces for simple applications, whereas general-purpose microprocessors
require additional chips to provide these functions, including at least one
ROM (read-only memory) chip to store the software
ABSTRACT
A touch screen is an electronic visual display that the user can control 
through simple or multi-touch gestures by touching the screen with one or
more fingers. Mainly used for remote controlling operations. Some touch screens
can also detect objects such as a stylus or ordinary or specially coated gloves.
In this project a touch screen PC is used as an input system to control the 
robot .The user input will be given to the GUI in touch screen PC.These inputs
will transmitted to the robot wirelessly using a Zigbee module. The angle of
servomotor can be attached to camera and can be adjusted  to record videos in 
different angle as an advanced version.
PROJECT DESCRIPTION
          In this project a touchscreen PC is used as an input system to 
          control the robot.The user input will be given to the GUI in touch 
          screen PC.These inputs will transmitted to the robot wirelessly using
          a Zigbeemodule. AVR microcontroller is used for controlling all the 
          operation of this section.This robot can be used for doing particular
          operations programmed in the robot. Controllers are not capable to 
          provide a voltage higher than 5v and  current greater than 20mA.So a
          normal motor rated 12v and 200mA require a driver circuit. L293D is a
          typical Motor driver or Motor Driver IC which allows DC motor to drive
          on either direction A servo motor is a dc, ac, or brushless dc motor
          combined with a position sensing device(e.g. a digital decoder). As 
          long as the coded signal exists on the input line, the servo will 
          maintain the angular position of the shaft. As the coded signal
          changes, the angular position of the shaft changes.Buzzer and LCD 
          are used as output showing devices buzzer will indicate obstacles
          presence by making sound and LCD will display information.UART ,is
          used foe serial communication and Zigbee/Bluetooth is used for 
          communication between transmitter section and receiver section.
Application:
Can be modified to detect Land Mines
Can be used my military in various spying operations.
Can be used in hazardous environment to measure gas concentration


