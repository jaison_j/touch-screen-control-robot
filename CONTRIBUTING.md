//TOUCH SCREEN CONTROLLED ROBOT
#include<LiquidCrystal.h>
// defines pins numbers
const int trigPin = A1;
const int echoPin = A0;
const int buzzPin = 12;
const int IN1=8;
const int IN2=9;
const int IN3=10;
const int IN4=11;
// defines variables
long duration;
int distance=0;
int buzzState = LOW;            
unsigned long previousMillis = 0;        
char mode;
LiquidCrystal lcd(2,3,4,5,6,7);

void setup() {
  
pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
pinMode(echoPin, INPUT); // Sets the echoPin as an Input
pinMode(buzzPin, OUTPUT);//buzzer pin as output
pinMode(IN1,OUTPUT);
pinMode(IN2,OUTPUT);
pinMode(IN3,OUTPUT);
pinMode(IN4,OUTPUT);
lcd.begin(16,2);
Serial.begin(9600); // Starts the serial communication
lcd.clear();
lcd.setCursor(5,0);
lcd.print("TSCR");
lcd.scrollDisplayLeft();
delay(1000);

}

void loop() {
 
  
int obstacle=ultra();

if(obstacle>=50 && obstacle<=60 )
{
  buzz(1200);
}
else if(obstacle>=40 && obstacle<=50)
{
  buzz(500);
}
else if(obstacle>=30 && obstacle<=40)
{
  buzz(250);
}
else if(obstacle<=30)
{
buzz(100);
bw();
delay(200);
mode='S'; 
}
else{
  delay(10);
  nobuzz();
}
switch(mode){

case 'F':fw();break;
case 'B':bw();break;
case 'L':lt();break;
case 'R':rt();break;
case 'S':st();break;
  
}
lcd.clear();
lcd.setCursor(0,0);
lcd.print("DISTANCE:");
lcd.setCursor(0,1);
lcd.print(obstacle);
//delay(200);
//mode='S';




}
void buzz(int interval)
{
unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
   
    previousMillis = currentMillis;

   
    if (buzzState == LOW) {
      buzzState = HIGH;
    } else {
      buzzState = LOW;
    }

    digitalWrite(buzzPin, buzzState);
  }
}
void nobuzz(){
    buzzState = LOW;
  digitalWrite(buzzPin, buzzState); 
}
int ultra()
{

// Clears the trigPin
digitalWrite(trigPin, LOW);
delayMicroseconds(2);
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);
// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin, HIGH);
// Calculating the distance in cm
distance= duration*0.034/2;
return distance;
  
}
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
   Serial.print(inChar);
    buzz(50);
    if(inChar=='1')
    {
      mode='F';
     
    }
    else if(inChar=='2')
    {
     mode='B';
    }
    else if(inChar=='3')
    {
     mode='L';
    }
    else if(inChar=='4')
    {
     mode='R';
    }
     else if(inChar=='5')
    {
     mode='S';
    }
    }
    
     
  
}
void fw()
{
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("forward");
    digitalWrite(IN1,HIGH);
    digitalWrite(IN2,LOW);
    digitalWrite(IN3,HIGH);
    digitalWrite(IN4,LOW);
   delay(100);
}
void bw()
{
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("backward");
    digitalWrite(IN1,LOW);
    digitalWrite(IN2,HIGH);
    digitalWrite(IN3,LOW);
    digitalWrite(IN4,HIGH);
  delay(100);
}
void rt()
{
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("right");
    digitalWrite(IN1,HIGH);
    digitalWrite(IN2,LOW);
    digitalWrite(IN3,LOW);
    digitalWrite(IN4,HIGH);
delay(100);
}
void lt()
{
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("left");
    digitalWrite(IN1,LOW);
    digitalWrite(IN2,HIGH);
    digitalWrite(IN3,HIGH);
    digitalWrite(IN4,LOW);
delay(100);
}
void st()
{

    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("stoping...");
    digitalWrite(IN1,LOW);
    digitalWrite(IN2,LOW);
    digitalWrite(IN3,LOW);
    digitalWrite(IN4,LOW);
delay(100);
}

